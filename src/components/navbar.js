import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import MenuIcon from "@mui/icons-material/Menu";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { Link } from "react-router-dom";
import { LanguagesSelect } from "./languages-select";

const drawerWidth = 240;
const navItems = [
  { id: "reservations", label: "Reservations", language: "en" },
  { id: "menu", label: "Menu", language: "en" },
  { id: "hours", label: "Opening Hours", language: "en" },
  { id: "visit", label: "Visit Us", language: "en" },
  { id: "reservations", label: "Reservas", language: "es" },
  { id: "menu", label: "Menú", language: "es" },
  { id: "hours", label: "Horario", language: "es" },
  { id: "visit", label: "Visítanos", language: "es" },
  { id: "reservations", label: "Reservierungen", language: "de" },
  { id: "menu", label: "Speisekarte", language: "de" },
  { id: "hours", label: "Öffnungszeiten", language: "de" },
  { id: "visit", label: "Besuch uns", language: "de" },
];

export const Navbar = (props) => {
  const { window, setLanguage, language } = props;
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState);
  };

  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: "center" }}>
      <Typography variant="h6" sx={{ my: 2 }}>
        {language === "en" || language === "de"
          ? "Dreams Restaurant"
          : "Restaurante Dreams"}
      </Typography>
      <Divider />
      <List>
        {navItems.map(
          (item) =>
            item.language === language && (
              <Link to={"/" + item.id}>
                <ListItem key={item.id} disablePadding>
                  <ListItemButton sx={{ textAlign: "center" }}>
                    <ListItemText primary={item.label} />
                  </ListItemButton>
                </ListItem>
              </Link>
            )
        )}
        <LanguagesSelect setLanguage={setLanguage} language={language} />
      </List>
    </Box>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <AppBar component="nav">
        <Toolbar className="flex justify-between bg-black p-5 shadow-2xl">
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            sx={{ mr: 2, display: { sm: "none" } }}
          >
            <MenuIcon />
          </IconButton>
          <Link to="/">
            <img
              src="/assets/logo-black.jpeg"
              title="Dreams Restaurant"
              alt="logo"
              width="120"
              height="120"
              className=""
            />
          </Link>
          <Box className="mb-5" sx={{ display: { xs: "none", sm: "block" } }}>
            {navItems.map(
              (item) =>
                item.language === language && (
                  <Link className="mx-4" to={"/" + item.id}>
                    <Button key={item.id} sx={{ color: "#fff" }}>
                      {item.label}
                    </Button>
                  </Link>
                )
            )}
            <LanguagesSelect setLanguage={setLanguage} language={language} />
          </Box>
        </Toolbar>
      </AppBar>
      <nav>
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: "block", sm: "none" },
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
            },
          }}
        >
          {drawer}
        </Drawer>
      </nav>
    </Box>
  );
};