import { Reservations } from "./reservations";

export const Home = ({language}) => {
  return (
    <div className="flex flex-col justify-center items-center h-full w-full">
     <Reservations language={language}/>
    </div>
  );
};
