export const Menu = ({language}) => {
  return (
    <div className="flex flex-col justify-center items-center h-full w-full p-5">
      <img title="menu" alt="menu" src={`/assets/menu/${language}.jpg`}/>
    </div>
  );
};
