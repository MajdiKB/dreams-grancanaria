import { MenuItem, Select } from "@mui/material";

export const LanguagesSelect = ({ language, setLanguage }) => {
  const languages = [
    { id: "de", label: "DE" },
    { id: "en", label: "EN" },
    { id: "es", label: "ES" },
  ];

  return (
    <Select
      value={language}
      className="mt-4 ml-4 xs:bg-transparent sm:bg-white xs:text-white sm:text-black h-8"
      onChange={(e) => setLanguage(e.target.value)}
      renderValue={(language) => {
        if (language)
          return languages.find((item) => item.id === language).label;
      }}
    >
      <MenuItem disabled value="">
        <em>
          {language === "en"
            ? "Change language"
            : language === "es"
            ? "Cambiar idioma"
            : "Sprache ändern"}
        </em>
      </MenuItem>
      {languages.map(
        (item) =>
          item.id !== language && (
            <MenuItem key={item.id} value={item.id}>
              {item.label}
            </MenuItem>
          )
      )}
    </Select>
  );
};
