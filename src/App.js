import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Home } from "./components/home";
import { Reservations } from "./components/reservations";
import { Menu } from "./components/menu";
import { Hours } from "./components/hours";
import { Visit } from "./components/visit";
import { useState } from "react";
import { Navbar } from "./components/navbar";

function App() {
  const [language, setLanguage] = useState("en");
  return (
    <div>
      <BrowserRouter>
        <Navbar setLanguage={setLanguage} language={language} />
        <div className="min-h-[100vh] pt-[180px] text-white" style={{ backgroundImage: "linear-gradient(black, grey)"}} >
          <Routes>
            <Route index element={<Home language={language} />} />
            <Route path="/" element={<Home language={language} />} />
            <Route
              path="/reservations"
              element={<Reservations language={language} />}
            />
            <Route path="/menu" element={<Menu language={language} />} />
            <Route path="/hours" element={<Hours language={language} />} />
            <Route path="/visit" element={<Visit language={language} />} />
            <Route path="*" element={<Home language={language} />} />
          </Routes>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
